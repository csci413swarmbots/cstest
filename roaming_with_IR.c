/*
  IR Roaming.c

  Use IR LEDs and IR receivers to detect obstacles while roaming.

*/

#include "simpletools.h"                        // Library includes
#include "abdrive.h"

int irLeft, irRight;                            // IR variables

int main()                                      // main function
{
  low(26);                                      // D/A0 & D/A1 to 0 V
  low(27);

  drive_setRampStep(10);                        // Max step 12 ticks/s every 20 ms

  while(1)
  {
    freqout(9, 1, 38000);                      // Check left & right objects
    irLeft = input(10);

    freqout(2, 1, 38000);
    irRight = input(3);

    if(irRight == 1 && irLeft == 1)             // No obstacles?
      drive_rampStep(128, 128);                 // ...full speed ahead
//    else if(irLeft == 0 && irRight == 0)        // Left & right obstacles?

  //    drive_rampStep(-128, -128);               // ...full speed reverse
    else if(irRight == 0)                       // Just right obstacle?
      drive_rampStep(-128, 128);                // ...rotate left
    else if(irLeft == 0)                        // Just left obstacle?
      drive_rampStep(128, -128);                // ...rotate right
  }
}