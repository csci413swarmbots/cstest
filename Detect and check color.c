/*
 * Bot will move forward until 12cms from an object then slowly move forward until
 * the color sensor gets a reading over 50 for any color. If the red is sensed over
 * 50 it will turn right (color we want) if it is not it will turn left (doesn't want
 * that color)
 */

#include "simpletools.h"                      // Include simpletools header
#include "abdrive.h"                          // Include abdrive header
#include "ping.h"                             // Include ping header
#include "colorpal.h"                         // Include colorpal

int turn;                                     // Navigation variable
colorPal *cpal;                               // Device identifier
int cpSIO = 2;                                // ColorPAL SIO -> Propeller P6
int r, g, b;                                  // Variables for red, green, blue
int i = 1;

int main()                                    // main function
{
  drive_setRampStep(10);                      // 10 ticks/sec / 20 ms
  drive_ramp(128, 128);                       // Forward 2 RPS

  // While disatance greater than or equal
  // to 20 cm, wait 5 ms & recheck.
  while(ping_cm(8) >= 12) pause(5);           // Wait until object in range

  
  drive_ramp(0, 0);                           // Then stop
  pause(1500);
  cpal = colorPal_open(cpSIO);                // Open connection to ColorPAL 
  
  while(i=1)                                    // Main loop
  {
    colorPal_getRGB(cpal, &r, &g, &b);        // Get red/green/blue readings
                   
    if(r > 50 || g > 50 || b > 50)
    {
      if( r >= 50)
      {
        drive_speed(64, -64);                     // rotate right
        
        // Keep turning while object is in view
        while(ping_cm(8) < 20);                     // Turn till object leaves view
        drive_ramp(0, 0);                           // Stop & let program end
      }
      else
      {
        drive_speed(-64, 64);                     // rotate left
        // Keep turning while object is in view
        while(ping_cm(8) < 20);                     // Turn till object leaves view
        drive_ramp(0, 0);                           // Stop & let program end
      }                
      
    }
    else
    {
      while(ping_cm(8) > 2)
      {
        drive_ramp(25, 25); // forward slow
      }        
      drive_ramp(0, 0);
    } 
    // break outter loop
    i = 0;                    
    pause(1000);                              // 1 s before repeat
  }


}
