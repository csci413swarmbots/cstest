/*
  Test IR Detectors.c
*/

#include "simpletools.h"                        // Include simpletools

int irLeft, irRight;                            // Global variables

int main()                                      // main function
{
  low(26);                                      // D/A0 & D/A1 to 0 V
  low(27);

  while(1)                                      // Main loop
  {
    freqout(9, 1, 38000);                      // Left IR LED light
    irLeft = input(10);                         // Check left IR detector

    freqout(2, 1, 38000);                       // Repeat for right detector
    irRight = input(3);

    print(" irLeft = %d, irRight = %d \n",       // Display detector states
                 irLeft,       irRight);
    pause(100);                                 // Pause before repeating
  }
}