/*
    This is a generic prototype for SwarmBots Assist Bots, which will be tested on
    Assist Bot 1, but include variations for the other two Assist Bots.
    Assist Bot Updated by Eugene, Wade, Scott, and Sadie.
    
    Problems upon beginning this sprint on Assist Bot code 4/25/2016: 
      version 1: The assist bots exhibit not clearly explainable run time errors in the form of logical errors. 
            Because of this, real time demonstration has become increasingly difficult. 
      version 2: This prototype is intended to correct that issue, as well as deal with limited "smoothness" that 
            occurs in the turning process.
      version 3: Another problem that will be addressed is the lack of multicore functionality.
      
      5/1/2016:
      version 1: Problems require direct hardware testing. 
      The version 3 oriented updates that have been added should relieve some of the probems version 1 sought to correct. 
      Version 2: the updates for "smoothness" will be changed last as they cannot be tested until the hardware functions properly.
      
      During this update 4/30/2016: Eugene has assisted in reconstructing Assist1 Bot's design by moving the horizontal Ultrasonic
            Ping))) sensor to a vertical position, which hypothetically (though suppositionally) should grant an increase
            in precision and accuracy on our distance measurements. For this reason, the differences in the success of
            our experiments may over exagerate the effectiveness of the changes in code.
            
      Update 5/1/2016: The code that allows for SD storage has been included, and then removed or commented out to save
            memory space while testing. The buffers will fit inside of the CMM multicog compact memory, but may have to 
            be altered once the XBEE comms code is fully integrated. The only clear way to resolve the lack of memory is
            to place some of the functions on the SD card and have them read out and recompiled on use. This is particularly
            useful for the "phasing", but cannot be implemented or tested until 
                A) Hardware functions. B) Comms function. C) The LeaderBot's sensors are repaired or replaced.
            -Wade

      Update 5/1/2016 # 2 at 20:52:
            Ethan, Scott, and I worked on the multicore aspects of the code and successfully ran a test of the colorPal, the
            ultrasound Ping))) sensor, and IRsensors working together where the colorPal was operating exclusively on a
            different core than the Ping))) (distance) sensor and the IRsensors. All of the sensors functioned, and were
            sending data, and the [bookFindingPhase() & followLeader()] processing core did succesfully communicate with the 
            [colorChecker() & checkForBook()] pro  ce  ssing core via volatile int values that a red object was found.
                
            A MAJOR CHANGE IN THIS CODE THAT TEAMMEMBERS SHOULD KNOW for testing is that there is now a timing delay associated with each
            processing core operation. The color sensor, Ping sensor, and IR sensors all now test for input only once every few
            hundred milliseconds. This removes much of the "overburdened processor induced errors" and successfully allowed for
            more sensor and multicore functionality. The exact delays are not optimized and may be altered. If someone optimizes them,
            please allow the rest of the team to know via comments during your next commit.
            
            A strange error with the IRsensors has begun to occur involving failure to recognize when the left IRsensor has
            or does not have something in front of it. Ethan and I attempted multiple fixes to resolve this issue and were
            unable to do so. We changed core operations, changed frequencies from 38k on the left to 37k, used a set of
            different colored, shaped, and textured objects to test for material effects, and were still unable to resolve this
            issue. Other than the left IR sensor, the first phase of the AssistBot code appears to be functioning properly
            as of this update. If you find any other errors, please note and document them.
            -Wade
            
      Update 5/2/2016:
            Changed comments sections so that using Visual Studios or Notepad++ will allow collapsing sections that are not actively
            being worked on. Changed "Function Definitions" comments to "Function Prototypes" in the "Function Prototypes subsection.
            Altered code so that the main core, called core [0] (the core in which the main() function operates) contains a printData
            function and that eases trouble shooting and should also assist in SD Card security logging and navigation.
            

*/

//updated 20:12 of 4/30/2016
//updated 20:52 of 5/01/2016
//updated 17:09 of 5/02/2016
/******************************************** Inclusions and Libraries **********************************************/
#include "simpletools.h"                        // simple tools library
#include "abdrive.h"                            // drive header
#include "ping.h"                               // Include ping header
#include "colorpal.h"                           // colorPAL header


/******************************************** Defines, Macros, Constants ********************************************/
#define IRLEFT 10
#define IRRIGHT 3
#define TRUE 1
#define FALSE 0
#define PAUSE pause(100);


/******************************************** Global Variables ******************************************************/

// Navigation variables
int irLeft, irRight;                            // IR variables
volatile int dist_Ping_Sensed;                  // PING))) variable

// Color sensor variables
colorPal *cpal;                        			// device identifier for color sensor
volatile int cpSIO = 0;                         // colorPAL input/output serial pin
volatile int foundBook = FALSE;                 // inter cog (multicore) global variable for removing arguments
volatile int r, g, b;       					// variables for red, green, and blue
      
// SD memory card variables
//FILE *fp;                                     // File pointer for SD card
//char str[512];                                // File buffer
//char cmdbuf[10];    // Command buffer
//char valbuf[4];     // Text value buffer
//int val;            // value

volatile int colorCheckerInitiated = FALSE;
volatile int checkForBookInitiated = FALSE;

/******************************************** Function Prototypes ************************************************/
void printData(int);
void checkForBook();
void followLeader();
void bookFindingPhase();
void colorChecker();
void bookRetrievalPhase();
// void returnToStart();
// void readFromFile();

/*______________________-End of function prototype declarations-_______________________*/ 


/******************************************** Main Program *********************************************************/

  int main()           
  { 
    // Max step 12 ticks/s every 20 ms
    drive_setRampStep(12);
    
    int *cog_colorChecker = cog_run(colorChecker, 128); PAUSE     // Run blink in other cog
	  int *cog_bookFinding = cog_run(bookFindingPhase, 128); PAUSE
    
    while(!foundBook)
    {
      printData(0);
      printData(1);
      printData(2);
      printData(3);
      PAUSE
    }
    cog_end(cog_colorChecker);
	  cog_end(cog_bookFinding);
	
    bookRetrievalPhase(); 
                                           
    return 0; //end of main();
  }

/**************************************** Function Definitions ********************************************/  
  
/************ ____________________ main core [0] function definitions ___________________________ ******************/

  void printData(int in)
  {
    int dataSet = in;
  	switch(dataSet)
  	{
      case 0: // if dataSet == 0 [ main functions ] core data is printed
        print("\nprintData(): case: 0 [main functions] core:\n");
        print("\t \n");
        break;
  		case 1: // if dataSet == 1 [ checkForBook() & colorChecker() ] core data is printed.
        print("\nprintData(): case: 1 [ checkForBook & colorChecker functions ] core:\n");
  			print("\tred: %04d, green: %04d, blue: %04d \t", r, g, b); 			// prints red, blue, and green readings to terminal
  			print("\tcheckForBook() cog init: %d \t", colorCheckerInitiated); 	// declares that checkForBook() function was initiated
  			print("\tcolorChecker() fun init: %d \n", checkForBookInitiated);   	// declares that colorChecker() function was initiated
  			break;
  		case 2: // if dataSet == 2 [ bookFinding() & followLeader() ] core data is printed.
        print("\nprintData(): case: 2 [ bookFinding & followLeader functions ] core:\n");
	      print("left: %d   right: %d   dist: %d \n", irLeft, irRight, dist_Ping_Sensed);
  			print("\t \n");
  			break;
  		case 3: // if dataSet == 3 [ comms function ] core data is printed.
        print("\nprintData(): case: 3 [ comms functions ] core:\n");
  		  print("\t \n");
  			break;
  	  default:
        print("\nprintData(): case: default\n");
  	}
    return;
  }
/*______________________-End of core [0] function definitions-_______________________*/ 

/************  checkForBook & colorChecker core [1] function definitions ******************/


  // defines cpal variable;c alls checkForBook function in a while loop until foundBook != 0.
  void colorChecker()                                   
  {
	// Open connection for ColorPAL sensor    
	  cpal = colorPal_open(cpSIO);
    while(!foundBook)
    {
      checkForBook();
    }
  }
  
  // second core assignment for 
  void checkForBook()
  { 
      // local variables for avoiding warnings and errors on colorPal_getRGB call.
      int re, gr, bl;
      
      // uses local variables and then assigns returns to volatile global variables
      colorPal_getRGB(cpal, &re, &gr, &bl);
      r = re; g = gr; b = bl;
 
      // checks for book, if red is scanned by colorPal, than foundBook (volatile) = TRUE.
      if( r > 50 || g > 50 || b > 50)
      {
        if ((r >= 50) && (g < 25) && (b < 50))
        {
          foundBook = TRUE;
        }         
      }
      PAUSE // Macro of pause(100); which allows easier updating of code.
      return;        
  }    
/*______________________-End of core [2] function definitions-_______________________*/ 
  
/************  bookFindingPhase & followLeader core [2] function definitions  ****************/

  void bookFindingPhase()
  { 
    // Local Variables: bookFindingPhase()
	
    // bookFindingPhase() while loop start
    while(!foundBook) 
    {     
      followLeader();                
    } // End of bookFindingPhase loop
    return;
  }  
  
  
  void followLeader()
  {       
	// Local Variables: followLeader()

    // Set frequency and pins for IR sensors/LEDs    
    freqout( IRLEFT - 1, 1, 38000 );                      
    freqout( IRRIGHT - 1, 1, 38000 );
    
	// Read the Infared sensor input and store into global variables
    irLeft = input( IRLEFT );
    irRight = input( IRRIGHT );
    
    // Distance is initialized by our ping sensors initial output regarding our Ping))) pin interface.
    dist_Ping_Sensed = ping_cm(8);
    
    // Checks if distance is greater than or equal to 6 cm.
    if(dist_Ping_Sensed >= 6)
    {
         if(( irRight == FALSE && irLeft == FALSE ) && ( dist_Ping_Sensed >= 9 ))
         { drive_ramp(24, -4); } 							// left & right, dist >=9, turn right                                
         else if( irLeft == TRUE )
		     {
            if( irRight == TRUE ){ drive_ramp(0, 0); } 	// If no object, left or right, then stop
		        else                 { drive_ramp(32, 4);}  // If no object left, object right, turn right
         }        
         else if( irRight == TRUE ) { drive_ramp(4, 32); }
		     else                    { drive_ramp(16, 16); }// If object, left and right, go forward.
    }
    else { drive_ramp(0, 0); } // if distance is less than 6, stop.
    								
	  PAUSE
    return;
  }  
/*______________________-End of core [2] function definitions-_______________________*/ 
 

/************ _________________ comms core [3] function definitions ___________________ ******************/


    // This is where our XBEE communications code should go when included.


/*______________________-End of core [3] function definitions-_______________________*/ 


  void bookRetrievalPhase()
  {
	   high(26);
	   high(27);
	   return;
  }  


  /*
  void readFromFile
  {
    This is for SD card based navigation and security logging. Not functional.
    //Sets up I/O pins for SD Files
    int DO = 22, CLK = 23;
    //Mount SD file system
    int DI = 24, CS = 25;
    sd_mount(DO, CLK, DI, CS);
    
    fp = fopen("navset.txt", "r");
    fread(str, 1, 512, fp);                    // navset.txt -> str
    int strLength = strlen(str);               // Count chars in str
    int i = 0;                                 // Declare index variable
    // this will print([sensor data], [ticks left], [ticks right]);
    fclose(fp);                                // Close SD file
  }


  void returnToStart()
  {
  	readFromFile();
  }

  */