/*
  IR Roaming.c

  Use IR LEDs and IR receivers to detect obstacles while roaming.

*/

#include "simpletools.h"                        // Library includes
#include "abdrive.h"

int irLeft, irRight;                            // IR variables

int main()                                      // main function
{
  low(26);                                      // D/A0 & D/A1 to 0 V
  low(27);

  drive_setRampStep(10);                        // Max step 12 ticks/s every 20 ms

  while(1)
  {
    freqout(9, 1, 38000);                      // Check left & right objects
    irLeft = input(10);

    freqout(2, 1, 38000);
    irRight = input(3);

    if(irRight == 0 && irLeft == 0)
      drive_rampStep(30, 30);
    else if(irLeft == 1 && irRight == 1)
     drive_rampStep(0, 0);
    else if(irRight == 1 && irLeft == 0)
      drive_rampStep(-64, 64);
    else if(irLeft == 1 && irRight == 0)
      drive_rampStep(64, -64);
  }
}