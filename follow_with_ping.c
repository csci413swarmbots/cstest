/*
 Follow with Ping.c
 Maintain a constant distance between ActivityBot and object.
*/
#include "simpletools.h" // Include simpletools header
#include "abdrive.h" // Include abdrive header
#include "ping.h" // Include ping header
int distance, setPoint, errorVal, kp, speed; // Navigation variables
int main() // main function
{
 setPoint = 10; // Desired cm distance
 kp = -10; // Proportional control
 drive_setRampStep(6); // 7 ticks/sec / 20 ms
 while(1) // main loop
 {
 distance = ping_cm(8); 
  errorVal = setPoint - distance;
  speed = kp * errorVal;
  print("%c", HOME);
  print("distance (%d) = ping_cm(8)%c\n\n", distance, CLREOL);
 if(speed > 128) speed = 128; // Limit top speed if(speed < -128) speed = -128;
 if (speed < -128) speed = -128;
 drive_rampStep(speed, speed); // Use result for following
 }
}